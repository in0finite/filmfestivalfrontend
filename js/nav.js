function loadHeader() {


}

function getCurrentLanguage() {

    var index = window.location.href.lastIndexOf('/');
    if(index < 2)
        return "RS";

    if(window.location.href[index - 1] == 'n')
        return "EN";

    return "RS";
}

function createNavigation() {

    if(getCurrentLanguage() == "RS") {
        addNavElement("O Festivalu", ['O Festivalu', 'Ziri', 'Nagrade', 'Sponzori', 'Kontakt']);
        addNavElement("Program", ['Filmovi', 'Mesta projekcije', 'Prodajna mesta']);
        addNavElement("Glumci");
        addNavElement("Vesti");
        addNavElement("Galerija");
        addNavElement("Omiljeni Filmovi");
    }
    else {
        addNavElement("About Festival", ['About Festival', 'Jury', 'Awards', 'Sponsors', 'Contact']);
        addNavElement("Program", ['Movies', 'Projection places', 'Sale places']);
        addNavElement("Actors");
        addNavElement("News");
        addNavElement("Gallery");
        addNavElement("Favorite movies");
    }

}


function addNavElement(displayName, elements = []) {

    //var container = document.getElementsByTagName("nav")[0].getElementsByTagName("ul")[0] ;
    var container = document.getElementById("myNavContainer");

    console.log("creating nav element: " + displayName);

    var content = "";

    // add start of li
    if (elements.length > 0) {
        content += '<li class="nav-item dropdown"> ';
    } else {
        content += '<li class="nav-item"> ';
    }

    // add dropdown button
    if (elements.length > 0) {
        content += '<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">'
            + displayName
            + '</a>';
    } else {
        content += '<a class="nav-link" href="' + camelize(displayName) + '.html" role="button" aria-haspopup="true" aria-expanded="false">'
            + displayName
            + '</a>';
    }

    if (elements.length > 0) {
        // add dropdown elements
        content += '<div class="dropdown-menu">';
        for (var i = 0; i < elements.length; ++i) {
            content += '<a class="dropdown-item" href="' + camelize(elements[i]) + '.html">' + elements[i] + '</a>';
        }
        content += '</div>';
    }

    // add end of li
    content += '</li>';


    container.innerHTML += content;
}

function camelize(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (letter) {
        return letter.toUpperCase();
    }).replace(/\s+/g, '');
}
