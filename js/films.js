

// FAVORITES

function addToFavorites(movie) {

    if(isInFavorites(movie))
        return;

    // var movies = getAllMoviesInFavorites();
    // movies.push(movie);
    //
    // setAllMoviesInFavorites(movies);

    var favorites = localStorage.getItem("favorites");
    if(null == favorites)
        favorites = "";
    favorites += movie + ";";
    localStorage.setItem("favorites", favorites);

}

function isInFavorites(movie) {

    return getAllMoviesInFavorites().indexOf(movie) >= 0;

}

function removeFromFavorites(movie) {

    if(!isInFavorites(movie))
        return;

    // var movies = getAllMoviesInFavorites();
    // movies.splice(movies.indexOf(movie), 1);
    //
    // setAllMoviesInFavorites(movies);

    var favorites = localStorage.getItem("favorites");
    if(null == favorites)
        favorites = "";
    favorites = favorites.replace(movie + ";", "");
    localStorage.setItem("favorites", favorites);

}

function getAllMoviesInFavorites() {

    var favorites = localStorage.getItem("favorites");
    if(null == favorites)
        return [];

    var movies = favorites.split(';');
    return movies;
}

function setAllMoviesInFavorites(movies) {

    var str = "";

    for (var i = 0; i < movies.length; i++) {
        str += movies[i] + ";";
    }

    localStorage.setItem("favorites", str);

}

// END FAVORITES


function getCurrentLanguage() {

    var index = window.location.href.lastIndexOf('/');
    if(index < 2)
        return "RS";

    if(window.location.href[index - 1] == 'n')
        return "EN";

    return "RS";
}

function getAllMoviesAndTheirData() {

    var movies = [];
    var oceanMovie;
    var jurassticWorldMovie;

    if (getCurrentLanguage() == "EN") {
        oceanMovie = ["Ocean's 8",
            "Oceans8.html",
            "oceans8",
            "Debbie Ocean gathers a crew to attempt an impossible heist at New York City's yearly Met Gala.",
            "Yugoslav FIlm Acrchive, Fountain, Cultural center",
            "Gary Ross",
            "Gary Ross, Olivia Milch",
            "Sandra Bullock, Cate Blanchett, Anne Hathaway",
            "../images/gallery/oceans8/thumbnails/oceans-8-sandra-bullock-first-look-1200x520.jpg"
        ];

        jurassticWorldMovie = ["Jurasstic World: Fallen Kingdom",
            "JurassticWorld.html",
            "jurassticWorld",
            "When the island's dormant volcano begins roaring to life, Owen and Claire mount a campaign to rescue the remaining dinosaurs from this extinction-level event.",
            "Yugoslav Film Acrchive, Fountain, Cultural center",
            "J.A. Bayona",
            "Colin Trevorrow, Derek Connoll",
            "Bryce Dallas Howard, Chris Pratt, Jeff Goldblum...",
            "../images/gallery/jurassticWorld/thumbnails/Jurassic-World-Fallen-Kingdom-Chris-Pratt-still.jpg"
        ];
    } else {
        oceanMovie = ["Okeanovih 8",
            "Oceans8.html",
            "oceans8",
            "Debbie Ocean okuplja posadu da pokuša nemoguće heist u godišnjoj Met Gala u Njujorku.",
            "Jugoslovenska kinoteka, Fontana, Dvorana kulturnog centra",
            "Gary Ross",
            "Gary Ross, Olivia Milch",
            "Sandra Bullock, Cate Blanchett, Anne Hathaway",
            "../images/gallery/oceans8/thumbnails/oceans-8-sandra-bullock-first-look-1200x520.jpg"
        ];

        jurassticWorldMovie = ["Svet jure: palo kraljevstvo",
            "JurassticWorld.html",
            "jurassticWorld",
            "Kada nestanak vulkana ostane u životu, Oven i Claire rade na kampanji za spasavanje preostalih dinosaurusa sa ovog događaja na nivou izumiranja.",
            "Jugoslovenska kinoteka, Fontana, Dvorana kulturnog centra",
            "J.A. Bayona",
            "Colin Trevorrow, Derek Connoll",
            "Bryce Dallas Howard, Chris Pratt, Jeff Goldblum...",
            "../images/gallery/jurassticWorld/thumbnails/Jurassic-World-Fallen-Kingdom-Chris-Pratt-still.jpg"
        ];
    }


    movies.push(oceanMovie);
    movies.push(jurassticWorldMovie);

    return movies;
}

function addAllMoviesToContainer(container) {

    var movies = getAllMoviesAndTheirData();
    //console.dir(movies);
    for (var i=0; i< movies.length; i++) {
        var movie = movies[i];
        //console.dir(movie);
        addMovieToContainer(container, ...movie);
    }

}

function addMovieToContainer(container, name, htmlFileName, titleForViewMovie, description, projectionPlace, director, scenarioWriters, actors, imgSrc) {

    // add new div
    //container.innerHTML += "<div class='moviecontainer'> </div>";
    var c = document.createElement("div");
    container.appendChild(c);
    c.className = "moviecontainer";
    //c.id = "moviecontainer_" + name;

    //var classes = $(".moviecontainer");
    //var c = classes[classes.length - 1];

    // load template into c

    loadHtml("MovieTemplate.html", c, () => {

        // modify elements values

        setMovieElementHtml(c, "movie_title", name);
        var selfLink = getMovieElementByClassName(c, "movie_selfLink");
        selfLink.href = "viewMovie.html?title=" + titleForViewMovie;
        setMovieElementHtml(c, "movie_description", description);
        setMovieElementHtml(c, "movie_director", director);
        setMovieElementHtml(c, "movie_scenarioWriters", scenarioWriters);
        setMovieElementHtml(c, "movie_actors", actors);
        var img = getMovieElementByClassName(c, "movie_imgSrc");
        img.src = imgSrc;

        // set html for favorite button
        var buttonHtml = "";
        if (getCurrentLanguage() == "RS") {
            if (isInFavorites(htmlFileName)) {
                buttonHtml = "Ukloni iz omiljenih <img src=\"../images/remove.png\" width=\"20\" />";
            } else {
                buttonHtml = "Dodaj u omiljene <img src=\"../images/favorites.png\" width=\"20\" />";
            }
        } else {
            if (isInFavorites(htmlFileName)) {
                buttonHtml = "Remove From Favorites <img src=\"../images/remove.png\" width=\"20\" />";
            } else {
                buttonHtml = "Add To Favorites <img src=\"../images/favorites.png\" width=\"20\" />";
            }
        }

        var button = getMovieElementByClassName(c, "movie_favoriteButton");
        button.innerHTML = buttonHtml;
        // set click handler
        button.onclick = () => {
            favoriteButtonClicked(c, htmlFileName);
        };

    });


    //console.log("added movie");

}

function setMovieElementHtml(movieDiv, className, html) {

    getMovieElementByClassName(movieDiv, className).innerHTML = html ;

}

function getMovieElementHtml(movieDiv, className) {

    return getMovieElementByClassName(movieDiv, className).innerHTML;

}

function getMovieElementByClassName(movieDiv, className) {

    var classes = movieDiv.getElementsByClassName(className);
    //console.log(classes);
    return classes.item('0');

}

function loadHtml(url, element, callback) {

    fetch(url).then((response) => (response.text())).then((html) => {
        element.innerHTML = html;
        callback();
    });

}


function favoriteButtonClicked(movieDiv, movieName) {

    if(isInFavorites(movieName)) {
        // remove it from favorites
        removeFromFavorites(movieName);
    } else {
        // add it to favorites
        addToFavorites(movieName);
    }

    // update button html - no need, we can refresh the page

    // reload page
    location.reload();

}


function sortMoviesByName(moviescontainer) {

    var sortedDivs = $(".moviecontainer").sort( (divA,divB) => {
        //console.dir(a);
        //console.dir(b);
        var title1 = getMovieElementHtml(divA, "movie_title");
        var title2 = getMovieElementHtml(divB, "movie_title");
        return compareStringsForSorting(title1, title2);
    } );

    //console.dir(sortedDivs);
    //moviescontainer.innerHTML = sortedDivs.html();
    $("#moviescontainer").html(sortedDivs);
}

function compareStringsForSorting(a, b) {

    var result = a.localeCompare(b);
    if(!isSortAscending())
        result = - result;

    return result;
}

function isSortAscending() {

    var dir = localStorage.getItem("sortDirection");
    return null == dir || dir == "normal" ;

}


